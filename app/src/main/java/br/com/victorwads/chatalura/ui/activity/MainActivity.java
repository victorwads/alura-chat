package br.com.victorwads.chatalura.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.victorwads.chatalura.R;
import br.com.victorwads.chatalura.model.Mensagem;
import br.com.victorwads.chatalura.services.ChatService;
import br.com.victorwads.chatalura.ui.adapter.MensagemAdapter;

public class MainActivity extends AppCompatActivity implements ChatService.Listenner {

    private TextView mensagemTexto;
    private ListView lista;
    private MensagemAdapter mensagemAdapter;
    private ChatService chatService;
    private EditText inputId;
    private ArrayList<Mensagem> mensagens = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        configLayout();
        chatService = new ChatService(this);
        chatService.setListenner(this);
        chatService.start();
    }

    private void configLayout() {
        inputId = findViewById(R.id.input_id_user);
        inputId.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                mensagemAdapter.setIdCliente(getUserID());
                return false;
            }
        });

        mensagemAdapter = new MensagemAdapter(this, getMensagems(), getUserID());
        lista = findViewById(R.id.listView);
        lista.setAdapter(mensagemAdapter);

        mensagemTexto = findViewById(R.id.input_message);
        mensagemTexto.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.e("Teste", "actionId: " + actionId + "; event: " + event);
                if (actionId == 6 || (event != null && (event.getKeyCode() == KeyEvent.FLAG_EDITOR_ACTION || event.getKeyCode() == KeyEvent.KEYCODE_ENTER))) {
                    sendMensage();
                    return false;
                }
                return true;
            }
        });

        Button enviar = findViewById(R.id.btn_send);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMensage();
            }
        });
    }

    private void sendMensage() {
        chatService.enviar(
                new Mensagem(getUserID(), mensagemTexto.getText().toString())
        );
        mensagemTexto.setText("");
    }

    private int getUserID() {
        int id = 1;
        try {
            id = Integer.parseInt(inputId.getText().toString());
        } catch (Exception e) {
            inputId.setText(String.valueOf(id));
        }
        return id;
    }

    @Override
    public void reciveMensagem(Mensagem mensagem) {
        mensagemAdapter.addItem(mensagem);
    }

    private ArrayList<Mensagem> getMensagems() {
        return this.mensagens;
    }

}
