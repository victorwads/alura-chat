package br.com.victorwads.chatalura.utils;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmptyCallback implements Callback {

    private static EmptyCallback instance;

    public static EmptyCallback instance() {
        if (instance == null)
            instance = new EmptyCallback();
        return instance;
    }

    @Override
    public void onResponse(Call call, Response response) {

    }

    @Override
    public void onFailure(Call call, Throwable t) {

    }
}
