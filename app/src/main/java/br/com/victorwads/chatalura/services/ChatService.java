package br.com.victorwads.chatalura.services;

import android.content.Context;

import br.com.victorwads.chatalura.model.Mensagem;
import br.com.victorwads.chatalura.utils.EmptyCallback;
import dagger.Module;
import dagger.Provides;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ChatService implements Callback<Mensagem> {

    final String URL = "http://10.0.0.151:8080/";
    private final Context context;
    private final ChatServiceInterface chatInterface;
    private Listenner listenner;
    private boolean running = false;
    private boolean wait = false;

    public ChatService(Context context) {
        this.context = context;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        chatInterface = retrofit.create(ChatServiceInterface.class);
    }

    public void start() {
        this.running = true;
        if (!wait) {
            recebe();
        }
    }

    public void stop() {
        this.running = false;
    }

    public void setListenner(Listenner listenner) {
        this.listenner = listenner;
    }

    public void enviar(final Mensagem mensagem) {
        chatInterface.enviar(mensagem).enqueue(EmptyCallback.instance());
    }

    private void recebe() {
        if (wait)
            return;
        wait = true;
        chatInterface.receber().enqueue(this);
    }

    @Override
    public void onResponse(Call<Mensagem> call, final Response<Mensagem> response) {
        if (response.isSuccessful()) {
            if (listenner != null) {
                listenner.reciveMensagem(response.body());
            }
        }
        next();
    }

    @Override
    public void onFailure(Call<Mensagem> call, Throwable t) {
        t.printStackTrace();
        next();
    }

    private void next() {
        wait = false;
        if (running) {
            recebe();
        }
    }

    /**
     * Listenner para vigiar o recebimento de novas mensagens.
     */
    public interface Listenner {
        void reciveMensagem(Mensagem mensagem);
    }
}
