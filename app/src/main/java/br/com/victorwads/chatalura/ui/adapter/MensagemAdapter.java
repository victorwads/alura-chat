package br.com.victorwads.chatalura.ui.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.victorwads.chatalura.R;
import br.com.victorwads.chatalura.model.Mensagem;

public class MensagemAdapter extends BaseAdapter {

    final Context context;
    final ArrayList<Mensagem> mensagens;
    private int idCliente;

    public MensagemAdapter(Context context, ArrayList<Mensagem> mensagens, int idCliente) {
        this.context = context;
        this.mensagens = mensagens;
        this.idCliente = idCliente;
    }

    @Override
    public int getCount() {
        return mensagens.size();
    }

    @Override
    public Object getItem(int position) {
        return mensagens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Mensagem item = mensagens.get(position);
        View layout = LayoutInflater.from(context).inflate(R.layout.mensagem, parent, false);


        if (idCliente == item.getId()) {
            View card = layout.findViewById(R.id.card_menssagem);
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) card.getLayoutParams();
            layoutParams.horizontalBias = 1;
            card.setLayoutParams(layoutParams);
        }

        TextView text = layout.findViewById(R.id.txt_mensagem);
        text.setText(item.getTexto());

        return layout;
    }

    public void addItem(Mensagem mensagem) {
        mensagens.add(mensagem);
        notifyDataSetChanged();
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdCliente() {
        return idCliente;
    }
}
